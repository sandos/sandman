#ifndef TOOLS_GETOPT_HEADER_FILE
#define TOOLS_GETOPT_HEADER_FILE

enum Flags
{
  NONE    = 0,
  SEARCH  = 1,
  FETCH   = 2,
  INSTALL = 3,
  REMOVE  = 4,
  CLEAN   = 5
};

int getopt(char *opt);

#endif
