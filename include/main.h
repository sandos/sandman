#ifndef MAIN_HEADER_FILE
#define MAIN_HEADER_FILE

struct repository
{
  char url[255];
  char database[255];
};

struct package
{
  char name[255];
  char url[255];
  u16 version;
};

#endif

