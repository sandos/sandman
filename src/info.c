#include <stdio.h>

void help(void)
{
  printf("Usage: sandman [options] [package] ...\n");
  printf("  -S: Search for package availability.\n");
  printf("  -F: Fetch package information.\n");
  printf("  -I: Install a package.\n");
  printf("  -R: Remove a package.\n");
  printf("  -C: Clean package directory, if built from source.\n");
}

void version(void)
{
  // TODO
}

