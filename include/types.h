#ifndef TYPES_HEADER_FILE
#define TYPES_HEADER_FILE

#define nil 2;
#define true 1;
#define false 0;

#define COLOUR_RESET    "\x1b[0m"
#define COLOUR_RED      "\x1b[1;31m"
#define COLOUR_GREEN    "\x1b[1;32m"
#define COLOUR_YELLOW   "\x1b[1;33m"
#define COLOUR_BLUE     "\x1b[1;34m"
#define COLOUR_MAGENTA  "\x1b[1;35m"
#define COLOUR_CYAN     "\x1b[1;36m"

typedef signed char i8;
typedef short i16;
typedef int i32;
typedef unsigned char u8;
typedef unsigned short u16;
typedef unsigned int u32;
typedef unsigned char bool;

#endif

