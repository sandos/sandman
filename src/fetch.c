#include "types.h"

#include <stdio.h>
#include <curl/curl.h>

int checkAvailability(char repository[], char package[])
{
  CURL *curl = curl_easy_init();

 /* "https://gitlab.com/sandos/sandman/-/raw/master/repositories/stable/sandman/package.conf" */
  curl_easy_setopt(curl, CURLOPT_URL, package.url);
  CURLcode result = curl_easy_perform(curl);

  if(result != CURLE_OK)
  {
    printf("curl_easy_perform() failed %s:\n", curl_easy_strerror(result));
    return 1;
  }

  curl_easy_cleanup(curl);

  return 0;
}

